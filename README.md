[TOC]


# Driving in the snow

via [Bill Wilhelm](https://www.flickr.com/photos/bilhelm96/5391694298/):


```mermaid
flowchart TD

start[ ]:::start --> nervous

nervous{{Are you nervous about driving in snowy weather?}}

at_home{{Are you at<br>home?}}

stay(Stay home):::finish

drive(Drive safe with extra<br>following distance):::finish

request(Get someone to drive<br>you home):::finish

nervous -->|No| drive
nervous -->|Yes| at_home

at_home -->|Yes| stay
at_home -->|No| request

classDef start fill:none,stroke:none;
classDef finish stroke-width:4px;

```

# Passing a vunerable road user coming the other way

```mermaid
flowchart TD

start[ ]:::start --> passing_distance

passing_distance{{Passing distance?}}

passing_distance -->|Less than 1.5 metres| stay

passing_distance -->|1.5 metres or more| go

go(Pass but be prepared to abort):::finish


stay(STOP and wait for<br>the VRU to pass):::finish

classDef start fill:none,stroke:none;
classDef finish stroke-width:4px;
```


# Overtake a slow moving vehicle

via [@jjrscott](https://twitter.com/jjrscott/status/1510934566105714694):

E.g. Cyclist, traction engine, horse, or other slow moving vehicle.


```mermaid
flowchart TD

start[ ]:::start --> centre

centre{{Centre markings?}}
passing_distance{{Passing distance?}}
their_speed{{Their speed?}}


centre -->|Dashed line| passing_distance

their_speed -->|10 mph or over| stay

centre -->|Double while lines| their_speed


passing_distance -->|Less than 1.5 metres| stay

passing_distance -->|1.5 metres or more| go

go(Overtake but be prepared to abort):::finish

their_speed -->|Under 10 mph| passing_distance

stay(Stay in lane<br>do NOT overtake):::finish

classDef start fill:none,stroke:none;
classDef finish stroke-width:4px;
```

[Highway Code rule 129](https://www.gov.uk/guidance/the-highway-code/general-rules-techniques-and-advice-for-all-drivers-and-riders-103-to-158#rule129):

> **Double white lines where the line nearest you is solid.** This means you **MUST NOT** cross or straddle it unless it is safe and you need to enter adjoining premises or a side road. You may cross the line if necessary, provided the road is clear, to pass a stationary vehicle, or overtake a pedal cycle, horse or road maintenance vehicle, if they are travelling at 10 mph (16 km/h) or less.
>
> **Laws [RTA 1988 sect 36](http://www.legislation.gov.uk/ukpga/1988/52/section/36) & [TSRGD schedule 9 part 8](https://www.legislation.gov.uk/uksi/2002/3113/schedule/9/made)**



# Traffic Lights

via [@roadtozero](https://twitter.com/roadtozero/status/1463054718125494272)

🔴 means stop

🔴 & 🟠 together mean stop – but get ready to go

🟠 on its own also means stop unless you’ve already crossed the stop line

🟢 means stop, unless the road is clear.









